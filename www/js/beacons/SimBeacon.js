var SimBeacon = function(beaconName, major, minor, aisle) {

    var self = this;

    this.initialize = function() {
        this.uuid = '6C562771-D25F-4015-A519-05C7B45F3C7F';
        this.beaconName = beaconName;
        this.major = major;
        this.minor = minor;
        this.aisle = aisle;
        this.distance = Math.floor(Math.random()*101);
        this.broadcast();
        return this;
    }

    this.broadcast = function() {
        // Randomly pick a distance between 1 and 100 (arbitrary unit of measurement) and broadcast it on intervals
        if (Math.random() < 0.5 || self.distance <= 1) {
            self.distance += 2;
        }
        else {
            self.distance -= 2;
        }
        //self.distance += Math.random() < 0.5 ? -1 : 1;
        //Math.floor(Math.random()*101);
        console.log(self.beaconName+" broadcasting from distance of "+self.distance);
    }
}