Installing Ionic/Cordova
=====================

## Using this project

We'll be using the `ionic` utility to run this project. The ionic utility is built on top of Cordova (the Apache foundation project that is the basis for PhoneGap). Ionic is the CSS/JS framework that I've used to build this application.

Check out Ionic docs here: ionicframework.com/docs/

## Installing Ionic command line utility

At a minimum, you need to install the ionic command line utility. In your terminal, run:

```bash
$ sudo npm install -g ionic
```

## Running on local browser

cd into project's root directory and in terminal:

```bash
$ ionic serve
```

The 'serve' command acts as a watcher in terminal. As files are updated and saved when working out of www directory, ionic/cordova will rebuild the application in your browser.

It's also useful for detecting configuration issues.

## Running on iOS emulator/device

cd into the project's root directory. To run on iOS emulator, we first need to build our application (first line) and then run it (second line):

```bash
$ ionic build ios
$ ionic emulate ios
```

To run on your iOS device, we need the following:
1. ios-deploy npm package
2. iOS developer license and xCode set up on your machine

```bash
$ npm install -g ios-deploy #installs ios-deploy tool globally
$ ionic build ios #builds the application
$ ionic run ios #installs and runs the application on plugged in device
```

***NOTE: Usually you only need to build once before any major changes. After the initial build, you can just use the 'ionic run/emulate ios' commands***

## Running on Android emulator/devices

Install Android ADT bundle (includes SDK) on your machine (http://developer.android.com/sdk/installing/bundle.html)

Once installed, set up your PATH environment variable:

```bash
$ touch ~/.bash_profile; open ~/.bash_profile
```
In the .bash_profile file, add the following:

export PATH=${PATH}:/PathToWhereverYouSavedSDK/android-sdk-macosx/platform-tools:/Development/android-sdk-macosx/tools

Where 'PathToWhereverYouSavedSDK' is...literally...the path to wherever you saved the ADT/SDK.

After you save the .bash_profile, execute it to update the PATH:

```bash
$ source ~/.bash_profile
```

To run the Android emulator, open ADT Eclipse and go to Window->Android Virtual Device Manager to add a virtual device.

Once added, run the following in terminal from project root:

```bash
$ ionic build android  #usually only needed once
$ ionic emulate android
```

***NOTE: if the initial build fails, it's likely you don't have Ant installed on your machine. You can install it using Homebrew: 

```bash
$ brew update
$ brew install ant
```
***

To run on Android device (I know, I know none of you snobs have Android devices...), make sure you have developer mode enabled and plugged in. Open terminal and run:

```bash
$ ionic build android
$ ionic run android
```

That's it!

## Debugging

Works just like debugging using Safari dev tools (for iOS only) and Chrome dev tools (for Android only).

## Using Sass (optional)

This project makes it easy to use Sass (the SCSS syntax) in your projects. This enables you to override styles from Ionic, and benefit from
Sass's great features.

Just update the `./scss/ionic.app.scss` file, and run `gulp` or `gulp watch` to rebuild the CSS files for Ionic.

Note: if you choose to use the Sass method, make sure to remove the included `ionic.css` file in `index.html`, and then uncomment
the include to your `ionic.app.css` file which now contains all your Sass code and Ionic itself:

```html
<!-- IF using Sass (run gulp sass first), then remove the CSS include above
<link href="css/ionic.app.css" rel="stylesheet">
-->
```

## Updating Ionic

To update to a new version of Ionic, open bower.json and change the version listed there.

For example, to update from version `1.0.0-beta.4` to `1.0.0-beta.5`, open bower.json and change this:

```
"ionic": "driftyco/ionic-bower#1.0.0-beta.4"
```

To this:

```
"ionic": "driftyco/ionic-bower#1.0.0-beta.5"
```

After saving the update to bower.json file, run `gulp install`.

Alternatively, install bower globally with `npm install -g bower` and run `bower install`.

