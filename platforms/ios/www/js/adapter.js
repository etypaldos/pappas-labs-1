//declare module
var adapter = angular.module('adapter', []);

adapter.MemoryAdapter = function() {

    var self = this;


    this.initialize = function() {
        // No Initialization required
        var deferred = $.Deferred();
        deferred.resolve();
        return deferred.promise();
    }

    // Define Aisles and what they contain
    // Estimate at the format a store will provide, needs to be parsed and converted so
    // each category
    var store = {
        beacons: [
            {
                aisle: 1,
                category: "produce"
            },
            {
                aisle: 2,
                category: "frozen"
            },
            {
                aisle: 3,
                category: "canned"
            },
            {
                aisle: 4,
                category: "meat"
            },
            {
                aisle: 5,
                category: "dairy"
            },
            {
                aisle: 6,
                category: "bakery"
            }
        ]
    };


    this.clearLocal = function() {
        localStorage.removeItem("shoppingList");
        //self.loadShoppingList();
    }

    this.loadShoppingList = function() {
        //localStorage.removeItem("shoppingList");
        var shoppingList = JSON.parse(localStorage.getItem("shoppingList")) || new Array();
        return shoppingList;
    }

    this.saveShoppingList = function(shoppingList) {
        //shoppingList = self.resetDefaults(shoppingList);
        localStorage.setItem("shoppingList", JSON.stringify(shoppingList));
    }

    this.loadIncompleteList = function() {
        var fullList = self.loadShoppingList();
        var filteredList = _.filter(fullList, function(item) {
            if (item["completed"] !== true) {
                return item;
            }
        });
        return filteredList;
    }

    this.loadCompletedList = function() {
        var fullList = self.loadShoppingList();
        var filteredList = _.filter(fullList, function(item) {
            if (item["completed"] === true) {
                return item;
            }
        });
        return filteredList;
    }

    this.deleteItemByName = function(itemName, shoppingList) {
        for(var i = 0; i < shoppingList["items"].length; i++) {
            if (shoppingList["items"][i]["item"] == itemName) {
                shoppingList["items"].splice(i, 1);
            }
        }
        return shoppingList;
    }

    this.resetDefaults = function(shoppingList) {
        var resetList = {
            "items": _.each(shoppingList["items"], function(elem, index, list) {
                elem.aisle = 'not set';
                elem.distance = 999;
            })
        }
        return resetList;
    }

    this.sortByDistance = function(shoppingList) {
        var sorted = _.sortBy(shoppingList["items"], function(item) {
            return item.distance;
        })
        return sorted;
    }

    this.sortByAisle = function(shoppingList) {
        var sorted = _.sortBy(shoppingList["items"], function(item) {
            return item.aisle;
        })
        return sorted;
    }

    this.matchListToBeacons = function(list) {
        self.sortBeaconsByDistance(self.beacons);
        for(var i = 0; i < list.length; i++) {
            var itemCat = list[i]["category"];
            for (var j = 0; j < self.beacons.length; j++) {
                if (itemCat === self.beacons[j]["beaconName"]) {
                    list[i]["aisle"] = self.beacons[j]["aisle"];
                    list[i]["distance"] = self.beacons[j]["distance"];
                }
            }
        }
        return list;
    }

    this.sortBeaconsByDistance = function(beacons) {
        // bubble sort
        var swapped;
        do {
            swapped = false;
            for (var i = 0; i < beacons.length-1; i++) {
                if (beacons[i].distance > beacons[i+1].distance) {
                    var temp = beacons[i];
                    beacons[i] = beacons[i+1];
                    beacons[i+1] = temp;
                    swapped = true;
                }
            }
        } while (swapped);
        self.beacons = beacons;
    }

    this.startBeacons = function() {
        // Create beacons
        self.beacons = new Array();
        self.createBeacons();

        //Set beacon change on interval
        self.beaconInterval = setInterval(function() {
            for (var i = 0; i < self.beacons.length; i++) {
                self.beacons[i].broadcast();
            }
        }, 12000);

    }

    this.stopBeacons = function() {
        clearInterval(self.beaconInterval);
    }

    this.createBeacons = function() {
        for (var i = 0; i < store.beacons.length; i++) {
            var $newBeacon = new SimBeacon(store.beacons[i]['category'], 1, i+1, store.beacons[i]['aisle']).initialize();
            self.beacons.push($newBeacon);
        }
        console.log("Created new set of beacons: "+self.beacons);
    }



    this.matchCategory = function(item) {
        if (item.toLowerCase().indexOf("frozen") > -1) {
            return "frozen";
        }
        else if (item.toLowerCase().indexOf("canned") > -1) {
            return "canned";
        }
        else if (item.toLowerCase().indexOf("bakery") > -1) {
            return "bakery";
        }
        else if (item.toLowerCase().indexOf("dairy") > -1) {
            return "dairy";
        }
        else if (item.toLowerCase().indexOf("bakery") > -1) {
            return "bakery";
        }
        else if (item.toLowerCase().indexOf("produce") > -1) {
            return "produce";
        }
        else if (item.toLowerCase().indexOf("meat") > -1) {
            return "meat";
        }
        else if (item.toLowerCase().indexOf("alcohol") > -1) {
            return "alcohol";
        }
        else {
            return "";
        }
    }


}