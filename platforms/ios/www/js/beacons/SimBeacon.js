var SimBeacon = function(beaconName, major, minor, aisle) {

    var self = this;

    this.initialize = function() {
        this.uuid = '6C562771-D25F-4015-A519-05C7B45F3C7F';
        this.beaconName = beaconName;
        this.major = major;
        this.minor = minor;
        this.aisle = aisle;
        this.broadcast();
        return this;
    }

    this.broadcast = function() {
        // Randomly pick a distance between 1 and 100 (arbitrary unit of measurement) and broadcast it on intervals
        self.distance = Math.floor(Math.random()*101);
        console.log(self.beaconName+" broadcasting from distance of "+self.distance);
    }
}