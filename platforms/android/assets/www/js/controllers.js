angular.module('konta.controllers', [])

.controller('AppCtrl', function($scope) {
})

.controller('ListCtrl', function($scope, $state, $interval, $timeout, $ionicModal, $ionicPopup, $ionicActionSheet) {
        $scope.categories = [
            'meat',
            'dairy',
            'produce',
            'seafood',
            'bakery',
            'canned',
            'frozen'
        ];
        //Load memory adapter
        var memoryAdapter = new adapter.MemoryAdapter();

        $scope.kntItems = memoryAdapter.loadShoppingList();

        $scope.kntItemForm = {};

        $scope.shopMode = false;

        $scope.createItem = function(kntItem) {
            // Make sure not empty
            if(kntItem.name.trim().length > 0) {
                if ($scope.kntItems.length > 0) {
                    var id = $scope.kntItems[$scope.kntItems.length-1]["id"]+1;
                }
                else {
                    var id = 1;
                }

                if (kntItem.category && kntItem.category.length > 0) {
                    var category = kntItem.category;
                }
                else {
                    var category = memoryAdapter.matchCategory(kntItem.name);
                }

                $scope.kntItems.push({
                    name: kntItem.name,
                    completed: false,
                    category: category,
                    aisle: 0,
                    distance: 999,
                    id: id
                })
                kntItem.name = "";
                memoryAdapter.saveShoppingList($scope.kntItems);
                $scope.categoryModal.hide();
            }
        }

        //Create item category model fn
        $scope.newItem = function(kntItem) {
            if(kntItem.name.trim().length > 0) {
                //copy item
                $scope.kntItemForm = angular.copy(kntItem);
                //set category
                $scope.kntItemForm.category = memoryAdapter.matchCategory(kntItem.name);
                //clear input
                kntItem.name = "";
                //if category matches, don't show modal
                if ($scope.kntItemForm.category && $scope.kntItemForm.category.length > 0) {
                    $scope.createItem($scope.kntItemForm);
                    return;
                }
                $('#addInput').blur();
                if (ionic.Platform.isIOS()) {
                    cordova.plugins.Keyboard.close();
                }

                $scope.categoryModal.show();
            }
        }

        // Create and load the category suggestion Modal
        $ionicModal.fromTemplateUrl('suggest-category.html', function(modal) {
            $scope.categoryModal = modal;
        }, {
            scope: $scope,
            animation: 'slide-in-up',
            backdropClickToClose: false
        });

        // Create and load the edit Modal
        $ionicModal.fromTemplateUrl('edit-item.html', function(modal) {
            $scope.taskModal = modal;
        }, {
            scope: $scope,
            animation: 'slide-in-up'
        });

        $scope.clearAll = function() {
            memoryAdapter.clearLocal();
            $scope.kntItems = memoryAdapter.loadIncompleteList();
            $scope.kntItemsCompleted = memoryAdapter.loadCompletedList();
        }



        $scope.editItem = function(kntItem) {
            //Copy object so user can cancel without updating 2-way binding
            $scope.kntItemForm = angular.copy(kntItem);
            // Open dialog
            $scope.taskModal.show();
        }

        $scope.saveItem = function(kntItemForm) {
            for(var i = 0; i < $scope.kntItems.length; i++) {
                if (kntItemForm.id === $scope.kntItems[i]["id"]) {
                    //Overwrite item
                    $scope.kntItems[i] = angular.copy(kntItemForm);
                    console.log($scope.kntItems[i]);
                    //Save
                    memoryAdapter.saveShoppingList($scope.kntItems);
                    //Hide modal
                    $scope.taskModal.hide();
                    //$scope.showSaveAlert();
                    $scope.showFlash('Successfully Saved!', 'bar-balanced');
                    return;

                }
            }
        }

        $scope.completeItem = function(kntItem) {
            kntItem.completed = true;
            memoryAdapter.saveShoppingList($scope.kntItems);
        }

        $scope.deleteItem = function(kntItem, notification) {
            for(var i = 0; i < $scope.kntItems.length; i++) {
                if (kntItem.id === $scope.kntItems[i]["id"]) {
                    //Delete item
                    $scope.kntItems.splice(i, 1);
                    //Save
                    memoryAdapter.saveShoppingList($scope.kntItems);
                    //Hide modal
                    $scope.taskModal.hide();
                    if (notification) {
                        $scope.showDeleteAlert();
                    }
                    else {
                        $scope.showFlash('Deleted!', 'bar-assertive')
                    }
                    return;
                }
            }
        }


        $scope.closeEditModal = function() {
            $scope.taskModal.hide();
        }

        $scope.uncompleteItem = function(kntItem) {
            kntItem.completed = false;
            memoryAdapter.saveShoppingList($scope.kntItems);
        }

        // An alert dialog
        $scope.showDeleteAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Successfully Deleted!'
            });
        };

        $scope.showSaveAlert = function() {
            var alertPopup = $ionicPopup.alert({
                title: 'Successfully Saved!'
            });
        };

        $scope.showAddedAlert = function(n) {
            var alertPopup = $ionicPopup.alert({
                title: 'Added '+n+' items!'
            });
        };

        // Triggered on a button click, or some other target
        $scope.showOptions = function() {

            // Show the action sheet
            var hideSheet = $ionicActionSheet.show({
                buttons: [
//                    { text: 'Check All' },
//                    { text: 'Uncheck All' },
//                    { text: 'Delete Checked Items' },
                    { text: 'Start Shop Mode', action: 'startShop' },
                    { text: 'Stop Shop Mode', action: 'stopShop' },
//                    { text: 'Start Beacons', action: 'startBeacons' },
//                    { text: 'Stop Beacons', action: 'stopBeacons' },
                    { text: 'Delete All Items', action: 'deleteAll' }
                ],
                titleText: 'Actions Menu',
                cancelText: 'Cancel',
                buttonClicked: function(index) {
                    if(this.buttons[index]['action'] === 'deleteAll') {
                        $scope.clearAll();
                    }
                    else if(this.buttons[index]['action'] === 'startBeacons') {
                        //start beacons
                        memoryAdapter.startBeacons();
                    }
                    else if(this.buttons[index]['action'] === 'stopBeacons') {
                        //stop beacons
                        memoryAdapter.stopBeacons();
                    }
                    else if(this.buttons[index]['action'] === 'startShop') {
                        $scope.startShopMode();
                    }
                    else if(this.buttons[index]['action'] === 'stopShop') {
                        $scope.stopShopMode();
                    }
                    return true;
                }
            });
        };

        $scope.syncList = function() {
            $scope.kntItems = memoryAdapter.matchListToBeacons($scope.kntItems);
            memoryAdapter.saveShoppingList($scope.kntItems);
        }

        $scope.startShopMode = function() {
            // start shop mode
            $scope.shopMode = true;
            //start beacons
            memoryAdapter.startBeacons();
            //sync distance/aisles
            if (typeof $scope.shopSyncInterval !== 'undefined') {
                $interval.cancel($scope.shopSyncInterval);
            }
            $scope.shopSyncInterval = $interval(function() {
                $scope.syncList();
                //$scope.$apply();
            }, 500);
        }

        $scope.stopShopMode = function() {
            //stop shop mode
            $scope.shopMode = false;
            if (typeof $scope.shopSyncInterval !== 'undefined') {
                clearInterval($scope.shopSyncInterval);
            }
            //stop beacons
            memoryAdapter.stopBeacons();
        }

        $scope.showFlash = function(action, flashClass){
            $scope.data = { showMe: true, action: action, flashClass: flashClass};

            $timeout(function() {
                $scope.data = { showMe: false};
            }, 2000);

        };


        // Item list methods

        $scope.submitItemListForm = function() {
            $scope.itemListForm.submit();
        }

        $scope.parseItemList = function(itemList) {
            var lines = itemList.replace(/\r\n/g, "\n").split("\n");
            var n = 0;
            for(var i = 0; i < lines.length; i++) {
                // Make sure not empty
                if(lines[i].trim().length > 0) {
                    if ($scope.kntItems.length > 0) {
                        var id = $scope.kntItems[$scope.kntItems.length-1]["id"]+1;
                    }
                    else {
                        var id = 1;
                    }
                    $scope.kntItems.push({
                        name: lines[i],
                        completed: false,
                        category: memoryAdapter.matchCategory(lines[i]),
                        aisle: 0,
                        distance: 999,
                        id: id
                    });
                    n++;
                }


            }
            memoryAdapter.saveShoppingList($scope.kntItems);
            $state.go('app.list');
            $scope.showAddedAlert(n);
        }

        $scope.goBack = function() {
            $ionicNavBarDelegate.back();
        }

})
